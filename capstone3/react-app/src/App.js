import {useState, useEffect} from 'react';
import './App.css';
import AppNavBar from './Components/AppNavBar';
import Registration from './Pages/Registration';
import Homepage from './Pages/Homepage';
import Logout from './Pages/Logout';
import Cart from './Pages/Cart';
import ServicePage from './Pages/ServicePage';
import Admin from './Pages/Admin';
import {UserProvider} from './UserContext'
import {ModalProvider} from './ModalContext'
import {ServiceProvider} from './ServiceContext'
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import AvailService from './Components/Modal'
import ServiceRegistration from './Components/ServiceRegistration'

function App() {

   const [user, setUser] = useState({
      id: null,
      isAdmin: null,
      isServiceProvider: null
   })

   const [serviceData, setServiceData] = useState({
      id: null,
      nickName: null
   
   })

   const [modal, setModal] = useState(false)

   //function for clearing localStorage on logout
   const unsetUser = () => {
      localStorage.clear();
   }

   useEffect(() => {
      fetch('https://blooming-oasis-83927.herokuapp.com/users/userDetails', {
         headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
         }
      })
      .then(res => res.json())
      .then(data => {
         if(typeof data._id !== "undefined"){
            setUser({
               id: data._id,
               isAdmin: data.isAdmin,
               isServiceProvider: data.isServiceProvider
            })
         }else{
            setUser({
               id: null,
               isAdmin: null,
               isServiceProvider: null
            })
         }
      })
   }, [])

  return (

//React context is nothing but a global state to the app. it is a way to make particular data available to all the components no matter how they are nested.
   <UserProvider value = {{user, setUser, unsetUser}}>
   <ModalProvider value = {{modal, setModal}}>
   <ServiceProvider value = {{serviceData, setServiceData}}>
   <div className="web">
    <Router >
       
       <Routes>
          <Route path= '/' element={<Registration/>}/>
          <Route path= '/homepage' element={<Homepage/>} />
          <Route path= '/logout' element={<Logout/>} />
          <Route path= '/modal' element={<AvailService/>} />
          <Route path= '/cart' element={<Cart/>} />
          <Route path= '/serviceRegister' element={<ServiceRegistration/>} />
          <Route path= '/servicePage' element={<ServicePage/>} />
          <Route path= '/adminPage' element={<Admin/>} />
          

       </Routes>
      
    </Router>
    </div>
    </ServiceProvider>
    </ModalProvider>
   </UserProvider>
   
  );
}

export default App;
