import {Card, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import Swal from 'sweetalert2';
import {useNavigate} from 'react-router-dom';


export default function CartCard(prop) {

	const [transaction, setTransaction] = useState({});
	const {nickName, totalPrice, duration, appliedOn, isPaid, transactionId} = transaction;
	const navigate = useNavigate();

	useEffect(() => {
	
			let data = prop.prop;
			const promise = Promise.resolve(data);
			promise.then(function(val){
				console.log(val);
				setTransaction(val);
			})
		
	}, [])

	function button(e){

		fetch(`https://blooming-oasis-83927.herokuapp.com/users/transaction/${transactionId}`, {
			headers: {
			      'Content-Type': 'application/json',
			       Authorization: `Bearer ${localStorage.getItem("token")}`
			    }
		})
		.then(res => res.json())
		.then(data => {

			if (data !== false) {

				Swal.fire({
				  title: 'Pay transaction?',
				  text: "You won't be able to revert this!",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Confir'
				})
				.then((result) => {
				  if (result.isConfirmed) {
				   	fetch(`https://blooming-oasis-83927.herokuapp.com/users/transaction/${transactionId}`, {
				   		method: 'PUT',
				   		headers: {
				   		      'Content-Type': 'application/json',
				   		       Authorization: `Bearer ${localStorage.getItem("token")}`
				   		    }
				   	})
				   	.then(res => res.json())
				   	.then(res => {
				   		if (res === true) {
				   			Swal.fire(
				   				'Success',
				   				'Your transaction is confirmed.',
				   			    'success'
				   				)
				   			.then((result) => {
				   				
				   				navigate('/homepage');
				   			})

				   		} else{
				   			Swal.fire(
				   				'Ooops',
				   				'There is a problem to your transaction. Please try again.',
				   			    'warning'
				   				)
				   		}
				   	})
				  }
				})
				
			} else {
				console.log(data)
				Swal.fire({
			  				icon: 'warning',
			  				title: 'Sorry an error occured.',
			  				text: 'Please try again!!'
			  			})
			}
		})

	
	
	}




	return(
		isPaid?
		<div>
			
		</div>
		:
		<Card>
		  <Card.Header as="h5">{nickName}</Card.Header>
		  <Card.Body>
		    <Card.Title>Total Payment</Card.Title>
		    <Card.Text>
		      {totalPrice}
		    </Card.Text>
		    <Card.Title>Duration</Card.Title>
		    <Card.Text>
		      {duration}
		    </Card.Text>
		    <Card.Title>Date</Card.Title>
		    <Card.Text>
		      {appliedOn}
		    </Card.Text>
		    <Button variant="primary" onClick={(e) => button(e)}>Check Out</Button>
		  </Card.Body>
		</Card>
		)
}