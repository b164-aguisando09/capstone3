import {Card, Button, Container} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import Swal from 'sweetalert2';
import {useNavigate} from 'react-router-dom';


export default function ActiveCard(prop) {

	const [services, setServices] = useState({})
	const {nickName, description, hobby, pricePerHour, _id} = services;
	const navigate = useNavigate();

	useEffect(() => {
		console.log(prop.prop)
		setServices(prop.prop)
	}, [])


	function button(e){

				Swal.fire({
				  title: 'Do you want to delete this Service?',
				  text: "You won't be able to revert this!",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Delete'
				})
				.then((result) => {
				  if (result.isConfirmed) {
				   	fetch(`https://blooming-oasis-83927.herokuapp.com/service/${_id}/archive`, {
				   		method: 'PUT',
				   		headers: {
				   		      'Content-Type': 'application/json',
				   		       Authorization: `Bearer ${localStorage.getItem("token")}`
				   		    }
				   	})
				   	.then(res => res.json())
				   	.then(res => {
				   		console.log(res)
				   		if (res === true) {
				   			Swal.fire(
				   				'Success',
				   				'Service Provider has been deleted.',
				   			    'success'
				   				)
				   			.then((result) => {
				   				
				   				navigate('/adminpage');
				   			})

				   		} else{
				   			Swal.fire(
				   				'Ooops',
				   				'There is a problem. Please try again.',
				   			    'warning'
				   				)
				   		}
				   	})
				  }
				})
				
			} 

	return(
		<Container>
		<Card>
		  <Card.Header as="h5">{nickName}</Card.Header>
		  <Card.Body>
		    <Card.Title>Description</Card.Title>
		    <Card.Text>
		      {description}
		    </Card.Text>
		    <Card.Title>Hobby</Card.Title>
		    <Card.Text>
		      {hobby}
		    </Card.Text>
		    <Button variant="primary" onClick={(e) => button(e)}>Delete</Button>
		  </Card.Body>
		</Card>
		</Container>
		)
}