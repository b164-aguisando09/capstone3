import {Container, Col, Row, Accordion, ListGroup} from 'react-bootstrap';
import {useState, useEffect} from 'react';


export default function AvailedServices(dataProp){

	const [isNull, setIsNull] = useState(false);
	const [transaction, setTransaction] = useState({});

	useEffect(() => {
		if (dataProp == null) {
			setIsNull(true);
		} else{

	
			let data = dataProp.dataProp;
			const promise = Promise.resolve(data);
			promise.then(function(val){
				console.log(val.nickName);
				setTransaction(val);
				
			})
		}
	}, [])

	

	
	const {nickName, totalPrice, isPaid, duration, appliedOn} = transaction;


	return(
		isNull ?
		<Container>
			<Row>
				<h1>No History</h1>
			</Row>
			
		</Container>
		:
		<Container>
		<Row>
			<Accordion>
			  <Accordion.Item eventKey="0">
			    <Accordion.Header>{appliedOn}</Accordion.Header>
			    <Accordion.Body>
			      <ListGroup as="ol">
			        <ListGroup.Item
			          as="li"
			          className="d-flex justify-content-between align-items-start"
			        >
			          <div className="ms-2 me-auto">
			            <div className="fw-bold">Name</div>
			         {nickName}
			          </div>
			        
			        </ListGroup.Item>
			        <ListGroup.Item
			          as="li"
			          className="d-flex justify-content-between align-items-start"
			        >
			          <div className="ms-2 me-auto">
			            <div className="fw-bold">Duration</div>
			            {duration}
			          </div>
			        
			        </ListGroup.Item>
			        <ListGroup.Item
			          as="li"
			          className="d-flex justify-content-between align-items-start"
			        >
			          <div className="ms-2 me-auto">
			            <div className="fw-bold">Total Payment</div>
			            {totalPrice}
			          </div>
			         
			        </ListGroup.Item>

			      
			        <ListGroup.Item
			          as="li"
			          className="d-flex justify-content-between align-items-start"
			        >
			          <div className="ms-2 me-auto">
			            <div className="fw-bold">Payment Status</div>
			            {
			            	isPaid ?
			            	"Already Paid"
			            	:
			            	"Not yet Paid"
			            }
			          </div>
			         
			        </ListGroup.Item>
			      </ListGroup>
			    </Accordion.Body>
			  </Accordion.Item>
			</Accordion>
			</Row>
		</Container>
		
		)
}