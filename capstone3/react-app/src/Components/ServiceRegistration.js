import {Button, Form, Modal, Container} from 'react-bootstrap';
import {useState, useContext, useEffect} from 'react';
import Swal from 'sweetalert2';
import ModalContext from '../ModalContext';
import ServiceContext from '../ServiceContext';
import {useNavigate} from 'react-router-dom';

export default function ServiceRegistration() {

 	
  const {serviceData} = useContext(ServiceContext);
  const [nickName, setNickName] = useState("");
  const [description, setDescription] = useState("");
  const [hobby, setHobby] = useState("");
  const [price, setPrice] = useState();
  const [isActive, setIsActive] = useState(false);
  const [show, setShow] = useState(false);
  let serviceId = serviceData.id;

 
  const navigate = useNavigate();




	function button(e){

		fetch('https://blooming-oasis-83927.herokuapp.com/service/addService', {
			method: 'POST',
			headers: {
			      'Content-Type': 'application/json',
			       Authorization: `Bearer ${localStorage.getItem("token")}`
			    },
			    body: JSON.stringify({
			      nickName: nickName,
			      description: description,
			     hobby: hobby,
			    pricePerHour: price
			    })
		})
		.then(res => res.json())
		.then(data => {
			if (data === true) {

				Swal.fire({
			  				icon: 'success',
			  				title: 'Registered Succefully.',
			  				text: 'Thank you!!'
			  			})
				.then(() => {
					navigate('/servicePage')
					setShow(false);
				})
			} else {
				console.log(data)
				Swal.fire({
			  				icon: 'warning',
			  				title: 'Sorry.',
			  				text: 'Please try again!!'
			  			})
			}
		})
	}


	useEffect(() => {
		if (nickName !== "" && description !== "" && hobby !== "" && price !== "") {
			setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [nickName, description, hobby, price])


  return (
    
      <Container>
          <Form>
            <Form.Group className="mb-3" controlId="date">
              <Form.Label>Set your Nickname</Form.Label>
             	<Form.Control 
             	  type="text" 
             	  placeholder="Enter Nickname"
             	  value={nickName}
             	/*target.value => the value inside the input field. when onChange is called*/
             	  onChange={e => setNickName(e.target.value)}
             	  required
             	   />
            </Form.Group>
         	<Form.Group className="mb-3" controlId="time">
         	  <Form.Label>Tell me about yourself</Form.Label>
         	  <Form.Control 
         	    as="textarea" rows={3}
         	    placeholder="Enter Description"
         	    value={description}
         	  /*target.value => the value inside the input field. when onChange is called*/
         	    onChange={e => setDescription(e.target.value)}
         	    required
         	     />
         	</Form.Group>
         	<Form.Group className="mb-3" controlId="hours">
         	  <Form.Label>State your hobbies</Form.Label>
         		<Form.Control 
         		  as="textarea" rows={3}
         		  placeholder="Enter Hobby"
         		  value={hobby}
         		/*target.value => the value inside the input field. when onChange is called*/
         		  onChange={e => setHobby(e.target.value)}
         		  required
         		   />
         	</Form.Group>
         	<Form.Group className="mb-3" controlId="hours">
         	  <Form.Label>Price per Hour</Form.Label>
         		<Form.Control 
         		  type='number'
         		  
         		  value={price}
         		/*target.value => the value inside the input field. when onChange is called*/
         		  onChange={e => setPrice(e.target.value)}
         		  required
         		   />
         	</Form.Group>
        
         	{
         		isActive ?
         		<Button variant="primary" onClick={e => button(e)}>
         		  Confirm
         		</Button>
         		:
         		  <Button variant="secondary" disabled>
         		  Confirm
         		</Button>	
         	}
          </Form>
       </Container>
   
  );
}
