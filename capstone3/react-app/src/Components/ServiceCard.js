import {Card, Button, Container} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import Swal from 'sweetalert2';
import {useNavigate} from 'react-router-dom';


export default function ServiceCard(prop) {

	const [user, setUser] = useState({})
	const {firstName, lastName, mobileNo, email, _id} = user;
	const navigate = useNavigate();

	useEffect(() => {
		console.log(prop.prop)
		setUser(prop.prop)
	}, [])


	function button(e){

				Swal.fire({
				  title: 'Convert into Service Provider?',
				  text: "You won't be able to revert this!",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Confirm'
				})
				.then((result) => {
				  if (result.isConfirmed) {
				   	fetch(`https://blooming-oasis-83927.herokuapp.com/users/createServiceProvider/${_id}`, {
				   		method: 'PUT',
				   		headers: {
				   		      'Content-Type': 'application/json',
				   		       Authorization: `Bearer ${localStorage.getItem("token")}`
				   		    }
				   	})
				   	.then(res => res.json())
				   	.then(res => {
				   		console.log(res)
				   		if (res === true) {
				   			Swal.fire(
				   				'Success',
				   				'Converted into a Service Provider.',
				   			    'success'
				   				)
				   			.then((result) => {
				   				
				   				navigate('/adminpage');
				   			})

				   		} else{
				   			Swal.fire(
				   				'Ooops',
				   				'There is a problem. Please try again.',
				   			    'warning'
				   				)
				   		}
				   	})
				  }
				})
				
			} 

	return(
		<Container>
		<Card>
		  <Card.Header as="h5">{firstName} {lastName}</Card.Header>
		  <Card.Body>
		    <Card.Title>Mobile Number</Card.Title>
		    <Card.Text>
		      {mobileNo}
		    </Card.Text>
		    <Card.Title>Email</Card.Title>
		    <Card.Text>
		      {email}
		    </Card.Text>
		    <Button variant="primary" onClick={(e) => button(e)}>Create</Button>
		  </Card.Body>
		</Card>
		</Container>
		)
}