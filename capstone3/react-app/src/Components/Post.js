import {Card, Button, Container, Modal, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';
import ModalContext from '../ModalContext'
import ServiceContext from '../ServiceContext'


export default function Post({postProp}){
	 
	const {setModal} = useContext(ModalContext);
	const {setServiceData} = useContext(ServiceContext);
	const {_id, nickName, description, hobby, pricePerHour} = postProp;
	function handleClick(e) {
	 	setModal(true);
	 	setServiceData({
	 		id: _id,
	 		nickName: nickName
	 	})
	 }
	 
	return(
		<Container className="m-2">
		<Card className = "card" >
			<Card.Body>
				<Card.Title>{nickName}</Card.Title>
				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Hobby</Card.Subtitle>
				<Card.Text>{hobby}</Card.Text>
				<Card.Subtitle>Price per hour:</Card.Subtitle>
				<Card.Text>Php {pricePerHour}</Card.Text>
				<Button variant="primary" className="m-2" onClick={handleClick}>Date Me!!</Button>
				<Card.Img variant="bottom" src="https://www.jcc-brooklyn.org/windsor-terrace/wp-content/uploads/sites/5/2018/05/girl_icon.jpg" className="fluid" />
				
				
			</Card.Body>
		</Card>

		</Container>
		)
}