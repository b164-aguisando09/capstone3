import AvailedServices from './Accordion';
import {useState, useEffect} from 'react';
import {Row} from 'react-bootstrap';

export default function Transaction() {


		const [transaction, setTransaction] = useState([]);
		const [data, setData] = useState([]);
		const [accordion, setAccordion] = useState([])

		

		useEffect(() => {
			fetch('https://blooming-oasis-83927.herokuapp.com/users/checkAllTransactions', {
			   method: 'PUT',
		       headers: {
		          Authorization: `Bearer ${localStorage.getItem("token")}`
		      			}
	       })
			.then(res => res.json())
			.then(data => {

				if (data.length == 0) {
					console.log(data);
					setTransaction(null);
				} else {
					console.log(data)
					setTransaction(data)
					
					
				}
			 
			})
		}, []);

		useEffect(() => {
			if (transaction == null) {
				setAccordion(() => {
					return(
						<AvailedServices key={data._id} dataProp={null}/>
						)
				})
			} else{
				setData(transaction.map(data => {
					const serviceData = data.availedService
					const serviceId = serviceData.serviceId;
					const user = data.userDetails
					const dateApplied = user.appliedOn
					let nickName = '';
					
					return (
						fetch(`https://blooming-oasis-83927.herokuapp.com/service/${serviceId}`)
						.then(res => res.json())
						.then(res => {
							nickName = res.nickName;
							console.log(res)
							return({
						nickName: nickName,
						duration: data.duration,
						totalPrice: data.totalPayment,
						isPaid: data.isPaid,
						id: serviceId,
						appliedOn: dateApplied
					})	
						})
					
						)
				}))
			}
		}, [transaction])

	useEffect(() => {
		console.log(data)
		if (transaction == null) {
			setAccordion(() => {
				return(
					<AvailedServices key={data._id} dataProp={null}/>
					)
			})
		} else{
			
			setAccordion(data.map(result => {
				return(
					<AvailedServices key={result.id} dataProp={result}/>
					)
			}))
		}

		
	}, [data])

	return(
		(transaction == null) ?
		<Row>
			<h1>No History</h1>
		</Row>
		:
		<Row>
		{accordion}
		</Row>
		)
}
