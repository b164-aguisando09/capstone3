
import {Container, Navbar, Nav, NavDropdown, Form, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {useState, Fragment, useContext, useEffect} from 'react';
import UserContext from '../UserContext'



export default function AppNavBar(){

	const {user} = useContext(UserContext);
	const [isCostumer, setIsCustomer] = useState("");

	useEffect(() => {
		console.log(user.isServiceProvider)
		let isAdmin = user.isAdmin;
		let isServiceProvider = user.isServiceProvider;
		if (isServiceProvider == true || isAdmin == true) {
			setIsCustomer(false)
		} else {
			setIsCustomer(true)
		}
	}, [])

	return(
		<Navbar bg="light" expand="lg" sticky='top'>
		  <Container>
		    <Navbar.Brand href="#">DateMe!!!</Navbar.Brand>
		    <Navbar.Toggle aria-controls="navbarScroll" />
		    <Navbar.Collapse id="navbarScroll" className="justify-content-end">
		      {
		      	isCostumer ?
		      	<Nav
		      	  className="justify-content-end"
		      	  style={{ maxHeight: '100px' }}
		      	  navbarScroll
		      	>
		      	<Nav.Link as={Link} to='/homepage'>Home</Nav.Link>
		      	<Nav.Link as={Link} to='/cart'>Cart</Nav.Link>
		      	<NavDropdown className="mr-5" title="" id="navbarScrollingDropdown">
		      	  <NavDropdown.Item as={Link} to="/logout">Log Out</NavDropdown.Item>
		      	  
		      	  <NavDropdown.Divider />
		      	  
		      	</NavDropdown>
		      	</Nav>
		      	:
		      	<Nav
		      	  className="justify-content-end"
		      	  style={{ maxHeight: '100px' }}
		      	  navbarScroll
		      	>
		      	<NavDropdown className="mr-5" title="" id="navbarScrollingDropdown">
		      	  <NavDropdown.Item as={Link} to="/logout">Log Out</NavDropdown.Item>
		      	  
		      	  <NavDropdown.Divider />
		      	  
		      	</NavDropdown>
		      	</Nav>
		      	
		      }
		        
		     
		   
		      
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
		)
}