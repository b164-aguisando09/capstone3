import {Card, Button, Container, Image} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';



export default function UserCard({userProp}){


	const {firstName, lastName, mobileNo, email} = userProp[0];
	console.log(userProp)
	const {city, province} = userProp[1];


	return(
		<Container className="mt-5 px-1">
		<Card >
			<Card.Body>
				<Card.Title>Profile</Card.Title>
				{/*<Card.Img variant="top" src="https://www.jcc-brooklyn.org/windsor-terrace/wp-content/uploads/sites/5/2018/05/girl_icon.jpg" className="round pb-2" />*/}
				<Image src="https://www.jcc-brooklyn.org/windsor-terrace/wp-content/uploads/sites/5/2018/05/girl_icon.jpg" fluid roundedCircle />
				<Card.Subtitle>Full Name</Card.Subtitle>
				<Card.Text>{`${firstName} ${lastName}`}</Card.Text>
				<Card.Subtitle>Address</Card.Subtitle>
				<Card.Text>{`${city}, ${province}`}</Card.Text>
				<Card.Subtitle>Contact Number</Card.Subtitle>
				<Card.Text>{mobileNo}</Card.Text>
				<Card.Subtitle>Email</Card.Subtitle>
				<Card.Text>{email}</Card.Text>
				
			
			</Card.Body>
		</Card>
		</Container>
		)
}