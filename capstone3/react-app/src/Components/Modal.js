import {Button, Form, Modal} from 'react-bootstrap';
import {useState, useContext, useEffect} from 'react';
import Swal from 'sweetalert2';
import ModalContext from '../ModalContext';
import ServiceContext from '../ServiceContext';
import {useNavigate} from 'react-router-dom';

export default function AvailService() {

  const {setModal} = useContext(ModalContext);
  const {serviceData} = useContext(ServiceContext);
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [hours, setHours] = useState("");
  const [show, setShow] = useState(true);
  const [isActive, setIsActive] = useState(false);
  let serviceId = serviceData.id;
  const handleClose = () => {
  	setShow(false)
  	setModal(false)
  };
  const handleShow = () => setShow(true);
  const navigate = useNavigate();




	function button(e){

		fetch('https://blooming-oasis-83927.herokuapp.com/users/availService', {
			method: 'PUT',
			headers: {
			      'Content-Type': 'application/json',
			       Authorization: `Bearer ${localStorage.getItem("token")}`
			    },
			    body: JSON.stringify({
			      serviceId: serviceId,
			      date: date,
			      time: time,
			      numberOfHours: hours
			    })
		})
		.then(res => res.json())
		.then(data => {
			if (data === true) {

				setDate("");
				setTime("");
				setHours("");

				Swal.fire({
			  				icon: 'success',
			  				title: 'Booked Succefully.',
			  				text: 'Thank you for booking me!!'
			  			})
				.then(() => {
					navigate('/homepage')
					setShow(false);
				})
			} else {
				console.log(data)
				Swal.fire({
			  				icon: 'warning',
			  				title: 'Sorry Schedule Unavailable.',
			  				text: 'Please book a different date!!'
			  			})
			}
		})

	
	
	}


	useEffect(() => {
		if (date !== "" && time !== "" && hours !== "") {
			setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [date, time, hours])


  return (
    <>
     
      <Modal show={show} onHide={handleClose} centered>
        <Modal.Header closeButton>
          <Modal.Title>Book Now!!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="date">
              <Form.Label>Set your Date</Form.Label>
              <Form.Control
                type="date"
                
                className="mx-2"
                value={date}
                /*target.value => the value inside the input field. when onChange is called*/
                onChange={e => setDate(e.target.value)}
                required
                autoFocus
              />
            </Form.Group>
         	<Form.Group className="mb-3" controlId="time">
         	  <Form.Label>Set your Time</Form.Label>
         	  <Form.Control
         	    type="time"
         	    className="mx-2"
         	    value={time}
         	    /*target.value => the value inside the input field. when onChange is called*/
         	    onChange={e => setTime(e.target.value)}
         	    required
         	  
         	  />
         	</Form.Group>
         	<Form.Group className="mb-3" controlId="hours">
         	  <Form.Label>Number of Hours</Form.Label>
         		<Form.Control as="select"
         		onChange={e => setHours(e.target.value)}>
         		  <option>Select Number of Hours</option>
         		  <option value="1">One</option>
         		  <option value="2">Two</option>
         		  <option value="3">Three</option>
         		  <option value="3">Four</option>
         		  <option value="3">Five</option>
         		  <option value="3">Six</option>
         		  <option value="3">Seven</option>
         		  <option value="3">Eight</option>
         		</Form.Control>
         	</Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          {
          	isActive ?
          	<Button variant="primary" onClick={e => button(e)}>
          	  Confirm
          	</Button>
          	:
          	  <Button variant="secondary" disabled>
          	  Confirm
          	</Button>	
          }
       
        </Modal.Footer>
      </Modal>
    </>
  );
}
