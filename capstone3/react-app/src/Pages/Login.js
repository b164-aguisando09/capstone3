import {Form, Button, Container, Navbar} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2';
import {Navigate, Link, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
const acc = {
	email: "admin@mail.com",
	password: "admin"
};

export default function Login(){

	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);
	const navigate = useNavigate();



	  function LoginUser(e) {

	 	e.preventDefault();

	 	setEmail("");
	 	setPassword("");

	 	//for fetch codes

	  	fetch('https://blooming-oasis-83927.herokuapp.com/users/login', {
	  			method: 'POST',
	  			headers: {
	  				'Content-Type': 'application/json'
	  			},
	  			body: JSON.stringify({
	  				email: email,
	  				password: password
	  			})
	  		})
	  		.then(res => res.json())
	  		.then(data => {
	  			
	  			if(typeof data.accessToken !== "undefined"){
	  				//store token to localStorage
	  				localStorage.setItem('token', data.accessToken)

	  				retrieveUserDetails(data.accessToken)
	  				setUser({
	  				   id: data._id,
	  				   isAdmin: data.isAdmin,
	  				   isServiceProvider: data.isServiceProvider
	  				});

	  				Swal.fire({
	  				      icon: 'success',
	  				      title: 'Login Successfully.',
	  				      text: 'Welcome to DateMe!!'
	  				    })
	  				.then((result) => {
	  					if (data.isServiceProvider == true) {
	  						
	  					navigate('/servicePage');
	  					
	  					} else if (data.isAdmin == true) {
	  						navigate('/adminPage');
	  					}
	  					else{
	  					navigate("/homepage")
	  					}
	  				})
	  			}else{

	  				Swal.fire({
	  				      icon: 'warning',
	  				      title: 'Login Failed',
	  				      text: 'Incorrect password or email address!'
	  				    });
	  			}
	  		})


	  		const retrieveUserDetails = (token) => {
	  			fetch('https://blooming-oasis-83927.herokuapp.com/users/userDetails', {
	  				headers: {
	  					Authorization: `Bearer ${token}`
	  				}
	  			})
	  			.then(res => res.json())
	  			.then(data => {
	  				console.log(data)
	  				setUser({
	  					id: data._id,
	  					isAdmin: data.isAdmin,
	  					isServiceProvider: data.isServiceProvider
	  				})
	  			})
	  		}


	}

	 useEffect(() => {
	  		    if (email !== "" && password !== "") {
	  		      setIsActive(true);
	  		    }else{
	  		      setIsActive(false);
	  		    }
	  		  }, [email, password])

	return(
		<Navbar bg="light" expand="lg">
			  <Container>
			    <Navbar.Brand as={Link} to="/homepage" >DateMe!!!</Navbar.Brand>
			    <Navbar.Toggle aria-controls="basic-navbar-nav" />
			  	
				<Form className="d-flex" onSubmit={(e) => LoginUser(e)}>
				            <Form.Control
				              type="email"
				              placeholder="Email Address"
				              className="mx-2"
				              value={email}
				              /*target.value => the value inside the input field. when onChange is called*/
				              onChange={e => setEmail(e.target.value)}
				              required
				              
				            />
				            <Form.Control
				              type="password"
				              placeholder="Password"
				              className="mx-2"
				              value={password}
				              /*target.value => the value inside the input field. when onChange is called*/
				              onChange={e => setPassword(e.target.value)}
				              required
				            />
				            {
				            	isActive ?

				            <Button variant="outline-success" type="submit">Login</Button>
				            	:
				            	  <Button variant="secondary" type="submit" id="submitBtn" disabled>
				            	  Login
				            	</Button>
				            }
				</Form>
			  </Container>
			</Navbar>
		)
}

