import {Container, Modal, Button, Form} from 'react-bootstrap'
import AppNavBar from '../Components/AppNavBar';
import {Fragment, useState, useEffect, useContext} from 'react';
import Post from '../Components/Post'
import UserCard from '../Components/userCard'
import ServiceRegistration from '../Components/ServiceRegistration'
import Swal from 'sweetalert2';
import ModalContext from '../ModalContext'
import {useNavigate} from 'react-router-dom';


export default function ServicePage(){

  const [post, setPost] = useState();
  const [data, setData] = useState({});
  const [address, setAddress] = useState({});
  const [nickName, setNickName] = useState("");
  const [description, setDescription] = useState("");
  const [hobby, setHobby] = useState("");
  const [price, setPrice] = useState();
  const [isActive, setIsActive] = useState(false);

  const {modal, setModal} = useContext(ModalContext);

  const userProp = [data, address];

  const [show, setShow] = useState(false);


  
 
 const navigate = useNavigate();

 const handleClose = () => {
  setShow(false)
  navigate('/logout')
 };
  useEffect(() => {

    

    fetch('https://blooming-oasis-83927.herokuapp.com/users/userDetails', {
       headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`
       }
    })
    .then(res => res.json())
    .then(res => {
      
      setData(res)
      setAddress(res.address)})
      
       

  }, [])
  useEffect(() => {

    fetch('https://blooming-oasis-83927.herokuapp.com/service/all')
    .then(res => res.json())
    .then(result => {
      
      console.log(data._id)

      for(let i = 0; i <= result.length; i++){
        let id = data._id;
        if (i < result.length) {
         console.log(data._id)
         console.log(result[i].userId)
          if (id == result[i].userId) {
            console.log('hello')
            setShow(false);
            break;
          } 
        } else if (i == result.length && id !== result[i-1].userId) {
            console.log("hi")
            setShow(true);
          }
      }
      }) 
  }, [data])

  useEffect(() => {
    if (nickName !== "" && description !== "" && hobby !== "" && price !== "") {
      setIsActive(true);
    } else{
      setIsActive(false);
    }
  }, [nickName, description, hobby, price])

  function button(e){

    fetch('https://blooming-oasis-83927.herokuapp.com/service/addService', {
      method: 'POST',
      headers: {
            'Content-Type': 'application/json',
             Authorization: `Bearer ${localStorage.getItem("token")}`
          },
          body: JSON.stringify({
            image: "image.png",
            nickName: nickName,
            description: description,
           hobby: hobby,
          pricePerHour: price
          })
    })
    .then(res => res.json())
    .then(data => {
      if (data === true) {

        Swal.fire({
                icon: 'success',
                title: 'Registered Succefully.',
                text: 'Thank you!!'
              })
        .then(() => {
          navigate('/servicePage')
          setShow(false);
        })
      } else {
        console.log(data)
        Swal.fire({
                icon: 'warning',
                title: 'Sorry.',
                text: 'Please try again!!'
              })
      }
    })
  }

    return(

        <Fragment className="home">

        <AppNavBar />
            <div className="con">
              <div className="left-col">
                 <UserCard key={data._id} userProp={userProp}/>
              
              </div>
              
              <div className="center-col">
                
               

              </div>
              
              <div className="right-col">
                
              </div>
            </div>

            <Modal show={show} onHide={handleClose} centered>
                    <Modal.Header closeButton>
                      <Modal.Title>Please Register</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                       <Form>
                         <Form.Group className="mb-3" controlId="date">
                           <Form.Label>Set your Nickname</Form.Label>
                            <Form.Control 
                              type="text" 
                              placeholder="Enter Nickname"
                              value={nickName}
                            /*target.value => the value inside the input field. when onChange is called*/
                              onChange={e => setNickName(e.target.value)}
                              required
                               />
                         </Form.Group>
                        <Form.Group className="mb-3" controlId="time">
                          <Form.Label>Tell me about yourself</Form.Label>
                          <Form.Control 
                            as="textarea" rows={3}
                            placeholder="Enter Description"
                            value={description}
                          /*target.value => the value inside the input field. when onChange is called*/
                            onChange={e => setDescription(e.target.value)}
                            required
                             />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="hours">
                          <Form.Label>State your hobbies</Form.Label>
                          <Form.Control 
                            as="textarea" rows={3}
                            placeholder="Enter Hobby"
                            value={hobby}
                          /*target.value => the value inside the input field. when onChange is called*/
                            onChange={e => setHobby(e.target.value)}
                            required
                             />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="hours">
                          <Form.Label>Price per Hour</Form.Label>
                          <Form.Control 
                            type='number'
                            
                            value={price}
                          /*target.value => the value inside the input field. when onChange is called*/
                            onChange={e => setPrice(e.target.value)}
                            required
                             />
                        </Form.Group>
                      
                       
                       </Form>
                    </Modal.Body>
                    <Modal.Footer>
                      {
                        isActive ?
                        <Button variant="primary" onClick={e => button(e)}>
                          Confirm
                        </Button>
                        :
                          <Button variant="secondary" disabled>
                          Confirm
                        </Button> 
                      }
                      
                    </Modal.Footer>
                  </Modal>

        </Fragment>
        )
}


