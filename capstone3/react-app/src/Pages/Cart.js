import {useState, useEffect} from 'react';
import {Row} from 'react-bootstrap';
import CartCard from '../Components/CartCard';
import AppNavBar from '../Components/AppNavBar';

export default function Cart() {

		const [transaction, setTransaction] = useState([]);
		const [dataTrans, setDataTrans] = useState([]);
		const [isNull, setIsNull] = useState(true);
		const [cart, setCart] = useState([]);

		

		useEffect(() => {
			fetch('https://blooming-oasis-83927.herokuapp.com/users/checkAllTransactions', {
			   method: 'PUT',
		       headers: {
		          Authorization: `Bearer ${localStorage.getItem("token")}`
		      			}
	       })
			.then(res => res.json())
			.then(res => {

				if (res.length == 0) {
					
					setTransaction(null);
				} else {
					for(let i = 0; i <= res.length; i++){
						if (i < res.length) {
							console.log(res[i].isPaid)
							if (res[i].isPaid == false) {

								setTransaction(res)
								break;
							} 
						} else if (i == res.length) {
							setTransaction(null)
						}
						
					}
					
					
					
				}
			 
			})
		}, []);

		useEffect(() => {
			console.log(transaction)
			if (transaction == null) {
				setIsNull(true);
			} else{

				setIsNull(false);
				setDataTrans(transaction.map(data => {
					const serviceData = data.availedService
					const serviceId = serviceData.serviceId;
					const user = data.userDetails
					const dateApplied = user.appliedOn
		
					
					return (
						fetch(`https://blooming-oasis-83927.herokuapp.com/service/${serviceId}`)
						.then(res => res.json())
						.then(res => {
							let nickName = res.nickName;
							let price = res.pricePerHour;
							return({
						nickName: nickName,
						duration: data.duration,
						totalPrice: data.totalPayment,
						isPaid: data.isPaid,
						id: serviceId,
						appliedOn: dateApplied,
						pricePerHour: price,
						transactionId: data._id
					})	
						})
					
						)
				}))
			}
		}, [transaction])

	useEffect(() => {
		console.log(dataTrans)
		if (transaction == null) {
			setIsNull(true);
		} else {
			setCart(dataTrans.map(result => {
				
					return(

						<CartCard key={result.id} prop={result}/>
						)
				
			
			}))
		}
		
	}, [dataTrans])


	return(
		isNull ?
		<Row>
			<AppNavBar/>
			<h1>Cart Empty</h1>
		</Row>
		:
		<Row>
		<AppNavBar/>
			{cart}
		</Row>
		)
}