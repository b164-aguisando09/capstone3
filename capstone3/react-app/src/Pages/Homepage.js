import {Container} from 'react-bootstrap'
import AppNavBar from '../Components/AppNavBar';
import {Fragment, useState, useEffect, useContext} from 'react';
import Post from '../Components/Post'
import UserCard from '../Components/userCard'
import AvailService from '../Components/Modal'
import Transaction from '../Components/Transactions'
import {useNavigate} from 'react-router-dom';
import ModalContext from '../ModalContext'
import UserContext from '../UserContext'



export default function Homepage(){

  const [post, setPost] = useState([]);
  const [data, setData] = useState({});
  const [address, setAddress] = useState({});
  const navigate = useNavigate();
  const {modal, setModal} = useContext(ModalContext);

  const userProp = [data, address];
  const {user} = useContext(UserContext);


 

  useEffect(() => {
    setModal(false);
    if (user.isServiceProvider == true) {
      navigate('/servicePage');
    } else if (user.isAdmin == true) {
      navigate('/adminPage')
    } else {
      fetch('https://blooming-oasis-83927.herokuapp.com/service/activeService')
      .then(res => res.json())
      .then(data => {
        console.log(data)
        setPost(data.map(services => {
          return(
            <Post key={services._id} postProp={services} />
            )
      }))
      })

      fetch('https://blooming-oasis-83927.herokuapp.com/users/userDetails', {
         headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`
         }
      })
      .then(res => res.json())
      .then(data => {
        setData(data)
        setAddress(data.address)}) 
    }
     

  }, [])

  console.log(address.city)

    return(

        <Fragment className="home">

        <AppNavBar />
            <div className="con">
              <div className="left-col">
                 <UserCard key={data._id} userProp={userProp}/>
              
              </div>
              
              <div className="center-col">
                {
                  (modal === true) ?
                  <AvailService/>
                  :
                  ""
                }
                {post}

              </div>
              
              <div className="right-col">
                <Transaction/>
              </div>
            </div>
        </Fragment>
        )
}


