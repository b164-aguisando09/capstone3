import {Container} from 'react-bootstrap'
import AppNavBar from '../Components/AppNavBar';
import {Fragment, useState, useEffect, useContext} from 'react';
import ServiceCard from '../Components/ServiceCard'
import ActiveCard from '../Components/ActiveCard'
import UserCard from '../Components/userCard'
import AvailService from '../Components/Modal'
import Transaction from '../Components/Transactions'
import {useNavigate} from 'react-router-dom';
import ModalContext from '../ModalContext'
import UserContext from '../UserContext'



export default function Admin(){

  const [card, setCard] = useState([]);
  const [services, setServices] = useState([]);
  const [data, setData] = useState({});
  const [address, setAddress] = useState({});
  const userProp = [data, address];

  useEffect(()=> {
    fetch(`https://blooming-oasis-83927.herokuapp.com/users/allUsers`, {
       headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`
       }
    })
    .then(res => res.json())
    .then(data => {
      setCard(data.map(users => {
    
        if (users.isServiceProvider == false) {
          if (users.firstName !== 'admin') {
            return (
              <ServiceCard key={users._id} prop={users} />
              )
          } 
          
        }
      }))
    })

    fetch(`https://blooming-oasis-83927.herokuapp.com/service/all`, {
       headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`
       }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      setServices(data.map(services => {
    
        if (services.isActive == true) {
            return (
              <ActiveCard key={services._id} prop={services} />
              )    
        }
      }))
    })

    fetch('https://blooming-oasis-83927.herokuapp.com/users/userDetails', {
       headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`
       }
    })
    .then(res => res.json())
    .then(data => {
      setData(data)
      setAddress(data.address)}) 
  }, [])


 

    return(

        <Fragment className="home">

        <AppNavBar />
            <div className="con">
              <div className="left-col">
                 <UserCard key={data._id} userProp={userProp}/>
              
              </div>
              
              <div className="center-col">
               <h1 className="text-center">Create Service Provider</h1>
               {card}

              </div>
              
              <div className="right-cols">
               <h1 className="text-center" >Delete Service</h1>
               {services}
              </div>
            </div>
        </Fragment>
        )
}


