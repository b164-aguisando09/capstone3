import {Container, Navbar, Nav, Form, Button, Col, Row} from 'react-bootstrap';
import {Link, useNavigate} from 'react-router-dom';
import {useState, useEffect, Fragment, useContext} from 'react';
import Swal from 'sweetalert2';
import Login from './Login';
import UserContext from '../UserContext';



export default function Registration(){

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [province, setProvince] = useState("");
	const [city, setCity] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);
	const [isPasswordSame, setIsPasswordSame] = useState(false);


	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	function RegisterUser(e) {

	  //prevents page redirection via a form submission
	  e.preventDefault();

	  fetch("https://blooming-oasis-83927.herokuapp.com/users/checkEmail", {
	    method: "POST",
	    headers: {
	          'Content-Type': 'application/json'
	        },
	        body: JSON.stringify({
	          email: email
	        })
	  })
	  .then(res => res.json())
	  .then(data => {
	    console.log(data)
	    if (data) {
	        Swal.fire({
	        icon: 'error',
	        title: 'Email already in use.',
	        text: 'Please use another email!'
	      });
	       
	      } else {
	     

	        fetch("https://blooming-oasis-83927.herokuapp.com/users/register",{
	          method:"POST",
	          headers: {
	            'Content-Type': 'application/json'
	        },
	        body: JSON.stringify({
	          firstName: firstName,
	          lastName: lastName,
	          city: city,
	          province: province,
	          email: email,
	          mobileNo: mobileNo,
	          password: password1
	        })
	        })
	        .then(res => res.json())
	        .then(data => {
	          if (data) {

	            //Clear input fields
	            setFirstName("");
	            setLastName("");
	            setProvince("");
	            setCity("");
	            setMobileNo("");
	            setEmail("");
	            setPassword1("");
	            setPassword2("");

	             Swal.fire({
	             icon: 'success',
	             title: 'Registered Successfully.',
	             text: 'Thank You for Registering!'
	            	           })
	            
	              

	          } else {
	              Swal.fire({
	              icon: 'error',
	              title: 'Something went wrong.',
	              text: 'Please try again!'
	            })
	          }
	        })
	      }
	  })

	}



	  useEffect(() => {
	    if (firstName !== "" && lastName !== null && city !== null && province !== null && mobileNo !== "" && email !== "" && password1 !== "" && password2 !== "") {
	      setIsActive(true);
	    }else{
	      setIsActive(false);
	    }
	  }, [firstName, lastName, city, province, mobileNo, email, password1, password2]);

	  useEffect(()=>{
	    if (password1 === password2) {
	      setIsPasswordSame(true);
	    }else{
	      setIsPasswordSame(false);
	    }
	  }, [password2])



	return(
	<Fragment>
		{/*login page*/}
		<Login/>

		<Container>
			<Col>
				<Row>
					<Form onSubmit= {(e) => RegisterUser(e)}>
					<h1 className="text-center">Register</h1>
					  {/*firstname*/}
					  <Form.Group 
					    className="mb-3" 
					    controlId="firstName">
					    <Form.Label>First Name</Form.Label>
					    <Form.Control 
					      type="text" 
					      placeholder="Enter First Name"
					      value={firstName}
					    /*target.value => the value inside the input field. when onChange is called*/
					      onChange={e => setFirstName(e.target.value)}
					      required
					       />
					  </Form.Group>
					{/*lastname*/}
					  <Form.Group 
					    className="mb-3" 
					    controlId="lastName">
					    <Form.Label>Last Name</Form.Label>
					    <Form.Control 
					      type="text" 
					      placeholder="Enter First Name"
					      value={lastName}
					    /*target.value => the value inside the input field. when onChange is called*/
					      onChange={e => setLastName(e.target.value)}
					      required
					       />
					  </Form.Group>
					 {/*address*/}
					<Form.Group 
					  className="mb-3" 
					  controlId="address">
					  <Form.Label>Address</Form.Label>
					  <div className="d-flex">
					  <Form.Control 
					  	className="mx-2"
					    type="text" 
					    placeholder="Enter Province"
					    value={province}
					  /*target.value => the value inside the input field. when onChange is called*/
					    onChange={e => setProvince(e.target.value)}
					    required
					     />
					     <Form.Control 
					       className="mx-2"
					       type="text" 
					       placeholder="Enter City"
					       value={city}
					     /*target.value => the value inside the input field. when onChange is called*/
					       onChange={e => setCity(e.target.value)}
					       required
					        />
					        </div>
					</Form.Group>
					{/*email*/}
					  <Form.Group 
					    className="mb-3" 
					    controlId="userEmail">
					    <Form.Label>Email address</Form.Label>
					    <Form.Control 
					      type="email" 
					      placeholder="Enter email"
					      value={email}
					    /*target.value => the value inside the input field. when onChange is called*/
					      onChange={e => setEmail(e.target.value)}
					      required
					       />
					    <Form.Text className="text-muted">
					      We'll never share your email with anyone else.
					    </Form.Text>
					  </Form.Group>
					  <Form.Group 
					    className="mb-3" 
					    controlId="mobileNo">
					    <Form.Label>Mobile Number</Form.Label>
					    <Form.Control 
					      type="tel" 
					      maxLength="11"
					      placeholder="Enter Mobile Number"
					      value={mobileNo}
					    /*target.value => the value inside the input field. when onChange is called*/
					      onChange={e => setMobileNo(e.target.value)}
					      required
					       />
					  </Form.Group>
					  <Form.Group className="mb-3" controlId="password1">
					    <Form.Label>Password</Form.Label>
					    <Form.Control 
					    type="password" 
					    placeholder="Password" 
					    value={password1}
					    onChange={e => setPassword1(e.target.value)}
					    required />
					  </Form.Group>
					   <Form.Group className="mb-3" controlId="password2">
					    <Form.Label>Verify Password</Form.Label>
					    <Form.Control 
					    type="password" 
					    placeholder="Password" 
					    value={password2}
					    onChange={e => setPassword2(e.target.value)}
					    required
					     />
					    {
					      (isPasswordSame && password1 !== "" && password2 !== "") ?
					       <Form.Text className="text-success">
					       Password Match
					      </Form.Text>
					     :
					      <Form.Text className="text-warning">
					       Password does not match
					     </Form.Text>
					    }
					  </Form.Group>
					  {
					    (isActive && isPasswordSame) ?
					      <Button variant="primary" type="submit" id="submitBtn">
					      Submit
					      </Button>
					  :
					      <Button variant="secondary" type="submit" id="submitBtn" disabled>
					      Submit
					      </Button>
					  }
					
					</Form>
				</Row>
			</Col>
		</Container>
	</Fragment>
		)
}