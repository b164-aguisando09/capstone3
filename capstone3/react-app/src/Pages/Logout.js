import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import {useContext, useEffect} from 'react'

export default function Logout(){

	const {unsetUser, setUser} = useContext(UserContext);

	unsetUser()

	
	//by adding useEffect this will allow the logout page to render first before triggering the useEffect which changes the state of our user
	useEffect(()=> {
		setUser({id:null, isAdmin:null, isServiveProvider:null})
	}, [])

	return(
		<Navigate to="/" />
		)
}