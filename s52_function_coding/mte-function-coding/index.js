function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    if (letter.length > 1) {
        return undefined;
    } else {
        for(let i = 0; i < sentence.length; i++){
            if (letter == sentence[i]) {
                result++;
            }
        }
        return result;
    }


}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

   
     let textLower = text.toLowerCase();
     let counter = 0;
     for (let i = 0; i < textLower.length; i++) {
       for (let n = i; n < textLower.length; n++) {
         if (textLower[i] === textLower[n + 1]) {
           counter += 1
         }
       }
     }
     if (counter > 0) {
        return false
    }else {

     return true
    }
   
    
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
    const discountedPrice = price * 0.80;
    const roundedOff = discountedPrice.toFixed(2);

    let string = discountedPrice.toString();
    if (age < 13) {
        return undefined;
    } else if (age >= 13 && age <=21) {
        return roundedOff;
    } else if (age >= 22 && age <= 64) {
       
       
        return price.toFixed(2);

    } else if (age > 64) {
        return roundedOff;
    }

    
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
  
    let hotCategories = [];
    let duplicate = true;
   if (hotCategories.length == 0) {
       for(let i = 0; i <= items.length; i++){
        
           if (i < items.length) {

               if (items[i].stocks == 0) {

                 if (hotCategories.length === 0) {
                     hotCategories.push(items[i].category)
                     
                 } else if (duplicate == false) {
                     hotCategories.push(items[i].category)
                    
                 } else{
                  
                   for(let n = 0; n <= hotCategories.length; n++){
                       if (n < hotCategories.length) {
                           if (hotCategories[n] == items[i].category) {
                               duplicate = true;
                               break;
                           } 
                       } else if (n == hotCategories.length){
                           duplicate = false;
                           
                       }  
                   }
                 }  
               }
           } else if(i == items.length) {
               return(hotCategories);
           }
           
       }
   }
    
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    let flyingVoters = [];
    if (flyingVoters.length == 0) {
        for(let i = 0; i <= candidateA.length; i++){
            if (i < candidateA.length) {
                for(let n = 0; n < candidateB.length; n++){
                    if (candidateA[i] == candidateB[n]) {
                        flyingVoters.push(candidateB[n])
                    }
                }
            } else if (i == candidateA.length) {
                return flyingVoters;
            }
           
        }
    }
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};